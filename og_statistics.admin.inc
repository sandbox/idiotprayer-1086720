<?php
// $Id: og_statistics.admin.inc $
/**
 * @file
 *   Admin settings for og_statistics module.
 */

function og_statistics_settings() {
  $form['rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild Organic Groups Statistics')
  );

  return $form;
}

function og_statistics_settings_recalc(&$context) {
  //variable_set('og_statistics_include_unapproved_members', $form_state['values']['include_unapproved_members']);
  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_group'] = 0;
	$context['sandbox']['max'] = db_query('SELECT COUNT(DISTINCT gid) FROM {og}')->fetchField();
  }
  
  $limit = 5;
  
  $result = db_select('og', 'gid')
    ->fields('gid')
	->condition('gid', $context['sandbox']['current_group'], '>')
	->orderBy('gid')
	->range(0, $limit)
	->execute();
	
  // Write a new record with calculated numbers for each one
  foreach ($result as $group) {
	
	$content_count = (int) db_query('SELECT COUNT(*) FROM {node} n INNER JOIN {field_data_group_audience} f ON n.nid = f.entity_id WHERE f.group_audience_gid = :gid AND f.entity_type = :type AND n.status = 1', array(':gid' => $group->gid, ':type' => 'node'))->fetchField();
	
	$member_count = (int) db_query('SELECT COUNT(*) FROM {field_data_group_audience} f INNER JOIN {users} u ON u.uid = f.entity_id WHERE f.group_audience_gid = :gid AND f.entity_type = :type AND u.status = 1', array(':gid' => $group->gid, ':type' => 'user'))->fetchField();
	
	$comments_count = (int) db_query('SELECT SUM(comment_count) FROM {node} n INNER JOIN {field_data_group_audience} f ON n.nid = f.entity_id LEFT JOIN {node_comment_statistics} ncs ON n.nid = ncs.nid WHERE f.group_audience_gid = :gid', array(':gid' => $group->gid))->fetchField();
	
	$last_node = (object) db_query('SELECT * FROM {field_data_group_audience} f INNER JOIN {node} n ON n.nid = f.entity_id WHERE f.entity_type = :type AND f.group_audience_gid = :gid ORDER BY f.group_audience_created DESC LIMIT 1', array(':gid' => $group->gid, ':type' => 'node'))->fetchObject();
	
	$last_member = (object) db_query('SELECT * FROM {field_data_group_audience} f INNER JOIN {users} u ON u.uid = f.entity_id WHERE f.entity_type = :type AND f.group_audience_gid = :gid ORDER BY f.group_audience_created DESC LIMIT 1', array(':gid' => $group->gid, ':type' => 'user'))->fetchObject();
	
	$last_comment = (object) db_query('SELECT * FROM {field_data_group_audience} f INNER JOIN {node_comment_statistics} ncs ON ncs.nid = f.entity_id WHERE f.group_audience_gid = :gid ORDER BY f.group_audience_created DESC LIMIT 1', array(':gid' => $group->gid))->fetchObject();

	db_insert('og_statistics')
      ->fields(array(
        'nid' => $group->etid,
        'gid' => $group->gid,
        'members_count' => $member_count,
    	'content_count' => $content_count,
	    'comments_count' => $comments_count,
	    'last_node_timestamp' => $last_node->group_audience_created,
	    'last_comment_timestamp' => $last_comment->last_comment_timestamp,
	    'last_member_timestamp' => $last_member->group_audience_created,
	    'last_comment_uid' => $last_comment->last_comment_uid,
	    'last_comment_nid' => $last_comment->nid,
	    'last_comment_cid' => $last_comment->cid,
	    'last_node_nid' => $last_node->nid,
	    'last_node_uid' => $last_node->uid,
	    'last_member_uid' => $last_member->uid,
      ))
      ->execute();
	
	$context['results'][] = $group->gid . ':' . $group->label;
	$context['sandbox']['progress']++;
	$context['sandbox']['current_group'] = $group->gid;
	$context['message'] = $group->label;
  }
  
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}
function og_statistics_settings_submit() {
  $batch = array(
    'title' => t('Generating statistics'),
    'operations' => array(
      array('og_statistics_settings_recalc', array()),
    ),
    'init_message' => 'Loading group nodes',
	'progress_message' => t('Calculated statistics for @current out of @total groups'),
    'error_message' => 'An unrecoverable error has occurred. You can find the error message below. It is advised to copy it to the clipboard for reference.',
    'finished' => 'og_statistics_settings_finished',
	'file' => drupal_get_path('module', 'og_statistics') . '/og_statistics.admin.inc',
  );
  batch_set($batch);
}
function og_statistics_settings_finished($success, $results, $operations) {
  if ($success) {
    $message = format_plural(count($results), 'One group processed.', '@count groups processed.');
  }
  else {
    $message = t('Finished with an error.');
  }
  drupal_set_message($message);
  // Providing data for the redirected page is done through $_SESSION.
  foreach ($results as $result) {
    $items[] = t('Loaded node %title.', array('%title' => $result));
  }
  $_SESSION['my_batch_results'] = $items;
}